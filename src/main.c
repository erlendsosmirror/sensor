/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <simpleprint/simpleprint.h>
#include <adt7410/adt7410.h>
#include <bmp280/bmp280.h>
#include <bme280/bme280.h>
#include <eeprom/24c256.h>
#include <ads1115/ads1115.h>
#include <ina3221/ina3221.h>
#include <ina226/ina226.h>
#include <ds1307/ds1307.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
#define LINEWIDTH 8

///////////////////////////////////////////////////////////////////////////////
// Usage
///////////////////////////////////////////////////////////////////////////////
int printusage ()
{
	SimplePrint (
"Usage: sensor [args]\n"
"  --help\n"
"    Print this message\n"
"  adt7410 <addr> <interval>\n"
"  bmp280 <addr> <interval>\n"
"  bme280 <addr> <interval>\n"
"    Test sensor with address and\n"
"    optional time interval.\n"
"  24c256 <addr> read <offset>\n"
"  24c256 <addr> write <offset> <data>\n"
"  24c256 <addr> busy\n"
"  ads1115 <addr> <interval> <ch>\n"
"  ina3221 <addr> <interval>\n"
"  ina226 <addr> <interval>\n"
"  ds1307 <interval>\n");
	return 0;
}

int printunknown (void)
{
	SimplePrint ("Invalid argument, use\n   sensor --help\nfor a usage list\n");
	return 1;
}

///////////////////////////////////////////////////////////////////////////////
// Common
///////////////////////////////////////////////////////////////////////////////
void Delay (int interval)
{
	struct timespec req;
	req.tv_sec = interval / 1000;
	req.tv_nsec = (interval % 1000) * 1000000;
	nanosleep (&req, 0);
}

void PrintLinePortion (unsigned int offs, unsigned char *data, unsigned int n)
{
	char str[16];
	char out[8+2+(LINEWIDTH*2)+1+LINEWIDTH+2];
	int i = 8;

	itoa_uint32_hex (offs, out);
	out[i++] = ':';
	out[i++] = ' ';

	for (int t = 0; t < n; t++)
	{
		itoa_uint32_hex (data[t], str);
		out[i++] = str[6];
		out[i++] = str[7];
	}

	out[i++] = ' ';

	for (int t = 0; t < n; t++)
	{
		char d = data[t];

		if (d < ' ' || d > '~')
			d = '.';

		out[i++] = d;
	}

	out[i++] = '\n';
	out[i++] = 0;
	SimplePrint (out);
}

///////////////////////////////////////////////////////////////////////////////
// ADT7410
///////////////////////////////////////////////////////////////////////////////
int TestADT7410 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 3)
	{
		SimplePrint ("Specify address [0, 3]\n");
		return 1;
	}

	const char addr[4] = {ADT7410_ADDRESS_0, ADT7410_ADDRESS_1, ADT7410_ADDRESS_2, ADT7410_ADDRESS_3};

	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Write default config
	if (ADT7410_WriteConfig (fd, 0, address))
		SimplePrint ("Write config failed\n");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Loop
	do
	{
		// Get temperature
		short temperature = 0;

		if (ADT7410_ReadTemp (fd, &temperature, address))
			SimplePrint ("Could not read temperature\n");

		// Print temperature
		char str[16];
		ADT7410_ToString (temperature, str);

		SimplePrint (str);
		SimplePrint ("\n");

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// BMP280
///////////////////////////////////////////////////////////////////////////////
int TestBMP280 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 1)
	{
		SimplePrint ("Specify address [0, 1]\n");
		return 1;
	}

	const char addr[2] = {BMP280_ADDRESS_0, BMP280_ADDRESS_1};
	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Get the ID of the device
	unsigned char id;

	if (BMP280_GetID (fd, address, &id))
		SimplePrint ("Failed to get ID\n");

	PrintValueHex ("id", id);

	// Set mode
	if (BMP280_SetMode (fd, address, BMP280_MODE_NORMAL | BMP280_PRESSURE_OVERSAMPLING_16 | BMP280_TEMP_OVERSAMPLING_16))
		SimplePrint ("Failed to set mode\n");

	// Get compensation variables
	BMP280_Comp comp;

	if (BMP280_GetCompensation (fd, address, &comp))
		SimplePrint ("Failed to get compensation\n");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Loop
	do
	{
		// Get status
		unsigned char status;

		if (BMP280_GetStatus (fd, address, &status))
			SimplePrint ("Failed to get status\n");

		PrintValueHex ("status", status);

		// Get measurements
		int measurements[2];

		if (BMP280_GetMeasurements (fd, address, measurements, &comp))
			SimplePrint ("Failed to get data\n");

		PrintValue ("temperature (1/100C)", measurements[0]);
		PrintValue ("pressure (Pa)", measurements[1] / 256);

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// BME280
///////////////////////////////////////////////////////////////////////////////
int TestBME280 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 1)
	{
		SimplePrint ("Specify address [0, 1]\n");
		return 1;
	}

	const char addr[2] = {BMP280_ADDRESS_0, BMP280_ADDRESS_1};
	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Get the ID of the device
	unsigned char id;

	if (BMP280_GetID (fd, address, &id))
		SimplePrint ("Failed to get ID\n");

	PrintValueHex ("id", id);

	// Set mode
	if (BME280_SetHumidMode (fd, address, BME280_HUM_OVERSAMPLING_2))
		SimplePrint ("Failed to set humidity mode\n");

	if (BMP280_SetMode (fd, address, BMP280_MODE_NORMAL | BMP280_PRESSURE_OVERSAMPLING_16 | BMP280_TEMP_OVERSAMPLING_16))
		SimplePrint ("Failed to set mode\n");

	// Get compensation variables
	BMP280_Comp comp;
	BME280_Comp comph;

	if (BMP280_GetCompensation (fd, address, &comp))
		SimplePrint ("Failed to get compensation\n");

	if (BME280_GetHumidCompensation (fd, address, &comph))
		SimplePrint ("Failed to get hum compensation\n");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Specify athmospheric pressure as 5th arg
	int atmos = (argc < 5) ? (101325*256) : (atoi(argv[4])*256);

	// Loop
	do
	{
		// Get status
		unsigned char status;

		if (BMP280_GetStatus (fd, address, &status))
			SimplePrint ("Failed to get status\n");

		PrintValueHex ("status", status);

		// Get measurements
		int measurements[3];

		if (BME280_GetMeasurements (fd, address, measurements, &comp, &comph))
			SimplePrint ("Failed to get data\n");

		PrintValue ("temperature (1/100C)", measurements[0]);
		PrintValue ("pressure (Pa)", measurements[1] / 256);
		PrintValue ("humidity (%)", measurements[2] / 1024);
		PrintValue ("height (1/10M)", -((measurements[1] - atmos) * 10) / (12*256));

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// 24C256
///////////////////////////////////////////////////////////////////////////////
int Test24C256 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 7)
	{
		SimplePrint ("Specify address [0, 7]\n");
		return 1;
	}

	const char addr[8] = {EEPROM_24C256_ADDR_0, EEPROM_24C256_ADDR_1,
		EEPROM_24C256_ADDR_2, EEPROM_24C256_ADDR_3,
		EEPROM_24C256_ADDR_4, EEPROM_24C256_ADDR_5,
		EEPROM_24C256_ADDR_6, EEPROM_24C256_ADDR_7};

	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Check argument
	if (argv[3] && !strcmp (argv[3], "read"))
	{
		int offset = (argc < 5) ? 0 : atoi (argv[4]);

		unsigned char data[64];

		if (EEPROM_24C256_Read (fd, address, offset, data, sizeof(data)))
			SimplePrint ("Failed to read\n");

		for (int i = 0; i < 64; i += 8)
			PrintLinePortion (offset + i, &data[i], 8);
	}
	else if (argv[3] && !strcmp(argv[3], "write"))
	{
		if (argc < 6)
			SimplePrint ("Specify offset and data to write\n");
		else
		{
			int offset = atoi (argv[4]);
			int len = strlen (argv[5]);

			if (len < 0 || len > 64)
				SimplePrint ("Data length must be within 64 bytes\n");

			if (EEPROM_24C256_Write (fd, address, offset, argv[5], len))
				SimplePrint ("Failed to write\n");
		}
	}
	else if (argv[3] && !strcmp(argv[3], "busy"))
	{
		int busy = 0;

		if (EEPROM_24C256_IsBusy (fd, address, &busy))
			SimplePrint ("Failed to write\n");

		if (busy)
			SimplePrint ("Appears to be busy\n");
		else
			SimplePrint ("Appears to be available\n");
	}
	else
		SimplePrint ("Specify read/write/busy\n");

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// ADS1115
///////////////////////////////////////////////////////////////////////////////
int TestADS1115 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 1)
	{
		SimplePrint ("Specify address [0, 1]\n");
		return 1;
	}

	const char addr[2] = {ADS1115_ADDR_0, ADS1115_ADDR_1};

	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Channel as 5th arg
	int ch = (argc < 5) ? 0 : atoi (argv[4]);

	// Loop
	do
	{
		// Write default config
		if (ADS1115_WriteConfig (fd, address, ch ? 0xB583 : 0x8583))
			SimplePrint ("Write config failed\n");

		// Conversion delay
		Delay (10);

		// Get result
		short res = 0;

		if (ADS1115_GetValue (fd, address, &res))
			SimplePrint ("Could not read result\n");

		PrintValue ("Result", res);

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// INA3221
///////////////////////////////////////////////////////////////////////////////
int TestINA3221 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 3)
	{
		SimplePrint ("Specify address [0, 3]\n");
		return 1;
	}

	const char addr[4] = {INA3221_ADDR_0, INA3221_ADDR_1, INA3221_ADDR_2, INA3221_ADDR_3};

	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Loop
	do
	{
		// Get result
		short res[7];

		for (int i = 0; i < 7; i++)
			if (INA3221_GetRegister (fd, address, i, (unsigned short*)&res[i]))
				SimplePrint ("Could not read result\n");

		PrintValue ("config", res[0]);
		PrintValue ("ch1 sv", res[1]);
		PrintValue ("ch1 bv", res[2]);
		PrintValue ("ch2 sv", res[3]);
		PrintValue ("ch2 bv", res[4]);
		PrintValue ("ch3 sv", res[5]);
		PrintValue ("ch3 bv", res[6]);

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// INA226
///////////////////////////////////////////////////////////////////////////////
int TestINA226 (int argc, char **argv)
{
	// Get address
	int address = (argc < 3) ? -1 : atoi (argv[2]);

	if (address < 0 || address > 15)
	{
		SimplePrint ("Specify address [0, 15]\n");
		return 1;
	}

	const char addr[16] = {INA226_ADDR_0, INA226_ADDR_1, INA226_ADDR_2, INA226_ADDR_3,
		INA226_ADDR_4, INA226_ADDR_5, INA226_ADDR_6, INA226_ADDR_7,
		INA226_ADDR_8, INA226_ADDR_9, INA226_ADDR_10, INA226_ADDR_11,
		INA226_ADDR_12, INA226_ADDR_13, INA226_ADDR_14, INA226_ADDR_15};

	address = addr[address];

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Read regs
	short regs[8];

	for (int i = 0; i < 8; i++)
		if (INA226_ReadRegister (fd, address, i, &regs[i]))
			SimplePrint ("Failed to read reg\n");

	for (int i = 0; i < 8; i++)
		PrintValue ("Reg", regs[i]);

	// Loop
	do
	{
		// Configure
		if (INA226_SetConfig (fd, address, 0x4127))
			SimplePrint ("Failed to configure\n");

		if (INA226_SetCalibration (fd, address, 512))
			SimplePrint ("Failed to configure\n");

		Delay (10);

		// Get result
		INA226_Values val;

		if (INA226_GetValues (fd, address, &val))
			SimplePrint ("Could not read result\n");

		PrintValue ("shunt voltage (2.5uV)", val.shuntvoltage);
		PrintValue ("bus voltage  (1.25mV)", val.busvoltage);
		PrintValue ("power         (0.1mA)", val.power);
		PrintValue ("current       (0.1mW)", val.current / 25);

		// Sleep
		if (interval)
			Delay (interval);
	} while (interval);

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// DS1307
///////////////////////////////////////////////////////////////////////////////
int TestDS1307 (int argc, char **argv)
{
	// Get address
	int address = DS1307_ADDR;

	// Open device
	int fd = open ("/dev/i2c", O_RDWR);

	if (fd < 0)
		return PrintError (argv[0], "/dev/i2c");

	// Specify interval as 4th arg
	int interval = (argc < 4) ? 0 : atoi (argv[3]);

	// Oscillator state
	char state = 0;

	if (DS1307_GetClockState (fd, address, &state))
		SimplePrint ("Failed to get state\n");

	if (state & 0x80)
	{
		SimplePrint ("Oscillator halted, enabling\n");

		if (DS1307_SetClockState (fd, address, state & 0x7F))
			SimplePrint ("Failed to set state\n");
	}
	else
		SimplePrint ("Oscillator running\n");

	// Loop reading if new time not specified
	if (!strcmp (argv[2], "get"))
	{
		do
		{
			// Get time
			char time[32] = {0};

			if (DS1307_GetTimeAsString (fd, address, time))
				SimplePrint ("Could not read time\n");

			SimplePrint (time);
			SimplePrint ("\n");

			// Sleep
			if (interval)
				Delay (interval);
		} while (interval);
	}
	else if (!strcmp (argv[2], "set"))
	{
		if (DS1307_SetTimeAsString (fd, address, argv[3]))
			SimplePrint ("Failed to set time\n");
	}
	else
		SimplePrint ("Specify get or set\n");

	close (fd);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Main function
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	if (argc >= 2)
	{
		if (!strcmp(argv[1], "--help"))
			return printusage ();
		else if (!strcmp(argv[1], "adt7410"))
			return TestADT7410 (argc, argv);
		else if (!strcmp(argv[1], "bmp280"))
			return TestBMP280 (argc, argv);
		else if (!strcmp(argv[1], "bme280"))
			return TestBME280 (argc, argv);
		else if (!strcmp(argv[1], "24c256"))
			return Test24C256 (argc, argv);
		else if (!strcmp(argv[1], "ads1115"))
			return TestADS1115 (argc, argv);
		else if (!strcmp(argv[1], "ina3221"))
			return TestINA3221 (argc, argv);
		else if (!strcmp(argv[1], "ina226"))
			return TestINA226 (argc, argv);
		else if (!strcmp(argv[1], "ds1307"))
			return TestDS1307 (argc, argv);
	}

	return printunknown ();
}
