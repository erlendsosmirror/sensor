###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = sensor
PROJECT_FILES = $(shell find src -name "*.*")

NOREL_BASE_TEXT = 0x2000E000
NOREL_BASE_RODATA = 0x20010800
NOREL_BASE_DATA = 0x20010C00
NOREL_BASE_BSS = 0x20011034

USR_INC = -I../../lib/libuser/inc -I../../lib/libsensor/inc
USR_LIB = -L../../lib/libuser/build -L../../lib/libsensor/build
USR_LIBS = -luser -lsensor

###############################################################################
# Compiler flags, file processing and makefile execution
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets
